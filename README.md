# Music Streamer

A simple draft for a BitTorrent-based P2P project.

## Setup

Run `npm i` and ensure it succeeds.

## Usage

Run `npm start`, to launch the browser version of the application.

Run `gulp help`, for a list of supported tasks.
